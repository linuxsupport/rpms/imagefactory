SPECFILE             = imagefactory.spec
SPECFILE_NAME        = $(shell awk '$$1 == "Name:"     { print $$2 }' $(SPECFILE) )
SPECFILE_VERSION     = $(shell awk '$$1 == "Version:"  { print $$2 }' $(SPECFILE) )
DIST                ?= $(shell rpm --eval %{dist})

clean:
	rm -rf build/ *.tgz

sources:
	tar -zcvf $(SPECFILE_NAME)-$(SPECFILE_VERSION).tgz --exclude-vcs --transform 's,^src/,$(SPECFILE_NAME)-$(SPECFILE_VERSION)/,' src/*
	mkdir -p build/tmp
	tar xvzf src/$(SPECFILE_NAME)-$(SPECFILE_VERSION).tar.gz -C build/tmp
	tar -zcvf imagefactory-plugins-$(SPECFILE_VERSION).tgz --exclude-vcs --transform 's,^\(src/\|build/tmp/$(SPECFILE_NAME)-$(SPECFILE_VERSION)/imagefactory_plugins/\),imagefactory-plugins-$(SPECFILE_VERSION)/,' build/tmp/$(SPECFILE_NAME)-$(SPECFILE_VERSION)/imagefactory_plugins/* src/*.patch
	cp imagefactory-plugins-$(SPECFILE_VERSION).tgz src/
	rm -rf build/tmp

rpm: sources
	rpmbuild -bb --define "dist $(DIST)" --define "_topdir $(PWD)/build" --define '_sourcedir $(PWD)/src' $(SPECFILE)
	rpmbuild -bb --define "dist $(DIST)" --define "_topdir $(PWD)/build" --define '_sourcedir $(PWD)/src' imagefactory-plugins.spec
	rm -rf src/imagefactory-plugins-$(SPECFILE_VERSION).tgz

srpm: sources
	rpmbuild -bs --define "dist $(DIST)" --define "_topdir $(PWD)/build" --define '_sourcedir $(PWD)/src' $(SPECFILE)
	rpmbuild -bs --define "dist $(DIST)" --define "_topdir $(PWD)/build" --define '_sourcedir $(PWD)/src' imagefactory-plugins.spec
	rm -rf src/imagefactory-plugins-$(SPECFILE_VERSION).tgz
