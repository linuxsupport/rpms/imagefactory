> **Warning**
> If you want to build imagefactory for a new OS, before create a new tag for it, please add the packages in koji for the new OS.

> The following commands should be run from aiadm or lxplus:

> `for i in testing qa stable; do koji add-pkg --owner=kojici linuxsupport<newOSdistag>-$i imagefactory; done`

> `for i in testing qa stable; do koji add-pkg --owner=kojici linuxsupport<newOSdistag>-$i imagefactory-plugins; done`
